from exchanges.bitfinex import Bitfinex
from slacker import Slacker
import os.path

prices = ["11800.0","12000.0","12500.0","13000.0","13500.0","14000.0","15000.0","16000.0","17000.0","18000.0","19000.0","20000.0"]

def slack(msg):
	slack = Slacker('putakey')
        slack.chat.post_message('#coins', text=msg, as_user=True)

for price in prices:
	current_price = Bitfinex().get_current_price()
	print "Current Price USD:", (current_price)
	if (float(current_price) > float(price)):
		fname = "." + str(price)
		if(os.path.isfile(fname)):
			break
		else:
			f = open("." + str(price),"w+")
			msg = "Bitcoin is now over " + str(price) + ". The current price is " + str(current_price)
			slack(msg)
